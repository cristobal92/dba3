/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dba3;

import com.eclipsesource.json.*;
import es.upv.dsic.gti_ia.core.ACLMessage;
import es.upv.dsic.gti_ia.core.AgentID;
import es.upv.dsic.gti_ia.core.SingleAgent;
import static java.lang.Math.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author jorge
 */
public class WorldManager extends SingleAgent {
    /*
    Clase para almacenar la posicion del agente y su rol
    */
    private class Agente{
        public final int bateriaInicial=100;
        public int rol,bateria,energiaGastada;
        public int x=0;
        public int y=0; 
        public JsonArray mapaAgente = new JsonArray();
        //Esto es para saber si el result del ultimo 
        public boolean ok=false;
        public boolean goal=false;
        public ArrayList<int[]> recorrido;
        
        public Agente(int x,int y,int rol){
            this.x=x;
            this.y=y;
            this.rol=rol;
            this.recorrido=new ArrayList<>();
            this.bateria=bateriaInicial;
        }
    }
    /*
    ATRIBUTOS
    */
    private MessageQueue cola;
    private String Key;
    private AgentID id,controlador;
    private int[][] mapa;
    //Cristobal: creo que es mejor hacer un hashmap usando como clave el nombre del agente y su info como valor
    //creo que es mas sencillo para saber si ya esta registrado
    HashMap<String,Agente> agentes;
    private ACLMessage inbox, outbox;
    private String world;
    private int energiaDisponible=1000;
    private boolean finalizado=false;
    //Distancia minima a la que deben estar los agentes
    //private final double distMinAgentes=5.0;
    private int goalX=-1, goalY=-1;
    
    private Interfaz interfaz;
    
    /*
    CONSTRUCTOR
    */
    public WorldManager(AgentID id,AgentID con,String w) throws Exception{
        super(id);
        this.id = id;
        this.outbox = new ACLMessage();
        this.world=w;
        this.controlador=con;
        this.cola = new MessageQueue(50);
        this.agentes=new HashMap();
        this.mapa=new int[501][501];
        //-1 igual a desconocido
        for (int i = 0; i < mapa.length; i++) {
            for (int j = 0; j < mapa.length; j++) {
                mapa[i][j]=-1;
            }
        }
        interfaz = new Interfaz();
    }
    //Funcion para actualizar el mapa del WM
    //Deberia funcionar con cualquier tipo de agente
    private void actualizarMapa(JsonArray radar, int x, int y){
        //El numero de filas y columnas coinciden 
        final int tamM=mapa.length;
        final int tamR=(int)sqrt(radar.size());
        /*
        El agente esta situado en el centro del mapa del radar, se obtienen unas 
        coordenadas en el mapa global donde comenzara a actualizarse
        */
        /*
        Las dimensiones son impares por lo que esta exactamente en la celda central,
        tamR se divide por dos, se trunca y se resta a las coordenadas en el mapa interno
        */ 
        int aux=(int)tamR/2;
        int i2 = y - aux;
        int j2 = x - aux;
        System.out.print("-------------------------------------------------------");
        //i,j->indices del radar
        //i2,j2->indices de la matriz mapa
        for (int i = 0; i < tamR && i2 < tamM; i++, i2++) {
            System.out.print("\n");
            for (int j = 0; j < tamR && j2 < tamM; j++, j2++) {
                if (i2 >= 0 && j2 >= 0) {
                    mapa[i2][j2] = radar.get(i * tamR + j).asInt();
                    if(esGoal(x, y)){
                        goalX=j2;
                        goalY=i2;
                    }
                    System.out.print(mapa[i2][j2]);
                }
            }
            j2 -= tamR;
        }
    }
    
    
    
    private boolean validarRecarga(String nombre){
        Agente ag=agentes.get(nombre);
        //Se resta a la bateria inicial la actual para obtener el gasto
        ag.energiaGastada+=ag.bateriaInicial-ag.bateria;
        return true;
    }
    //Devuelve false si con el movimiento esta muy cerca de otro, tambien actualiza la posicion del bot
    //COMPLETO
    private boolean validarMovimiento(String nom,String mov, int x, int y){
        Agente ag=agentes.get(nom);
        
        //move{N,S,E,W,NE,NW,SE,SW}
        switch(mov){
            case "moveN":
                y--;
                break;
            case "moveS":
                y++;
                break;
            case "moveE":
                x++;
                break;
            case "moveW":
                x--;
                break;
            case "moveNE":
                x++;
                y--;
                break;
            case "moveNW":
                x--;
                y--;
                break;
            case "moveSE":
                x++;
                y++;
                break;
            case "moveSW":
                x--;
                y++;
        }
        if(x<0 || x>=mapa[0].length)
            return false;
        if(y<0 || y>=mapa.length)
            return false;
        
        if( (ag.rol!=0 && mapa[y][x]==2) || mapa[y][x]==1)
            return false;
        //Calcula la distancia con el resto de agentes y se comprueba que no este demasiado cerca de otro
        for (Map.Entry<String, Agente> entry : agentes.entrySet()) {
            String nombre = entry.getKey();
            Agente agente= entry.getValue();
            //Si no es el mismo agente se comprueba
            if(!nombre.equals(nom) && x==agente.x && y==agente.y)
                return false;
            /*
            if(!nombre.equals(nom)){
                
                double X=x-agente.x;
                double Y=y-agente.y;
                double dist= sqrt(X*X+Y*Y);
                if(dist<distMinAgentes){
                    return false;
                }
            }
            */
        }
        if(ag.x!=x && ag.y!=y){
            ag.recorrido.add(new int[]{x,y});
        }
        //Actualizamos la posicion del agente
        ag.x=x;
        ag.y=y;
        return true;
            
    }
    //Funcion para realizar el subscribe, aqui se envia el mensaje de subscribe 
    // y se espera por la respuesta 
    //COMPLETO
    private void subscribe() throws Exception{
        //Se compone el mensaje para subscribe 
        JsonObject mensajeJson = new JsonObject();
        mensajeJson.add("world", world);
        outbox=new ACLMessage(ACLMessage.SUBSCRIBE);
        outbox.setSender(id);
        outbox.setReceiver(controlador);
        outbox.setContent(mensajeJson.toString());
        send(outbox);
        //Podriamos recibir un mensaje de los bots solicitando un checkin por
        // lo que esperamos un mensaje y si no es del controlador lo devolvemos
        // a la cola
        int cont=0;
        int max=20;//Se realizara esperarMensaje un numero maximo de veces
                   // si se supera se lanza una excepcion
        for(;cont>=0 && cont<max;cont++){
            esperarMensaje(true);
            inbox=cola.Pop();
            //detener el bucle (deberia ser el mensaje que se espera)
            if(inbox.getSender().name.equals(controlador.name))
                break;
            else{
                Thread.sleep(500);
                cola.Push(inbox);
            }
        }
        //Si no hay respuesta del controller se lanza una excepcion para detener la ejecucion en execute()
        if(cont>=max){
            throw new Exception("No hay respuesta del controller a la peticion subscribe");
        }
        switch(inbox.getPerformativeInt()){
            case ACLMessage.NOT_UNDERSTOOD:
                throw new Exception("NOT_UNDERSTOOD: "+getResult(inbox.getContent()));
            case ACLMessage.FAILURE:
                throw new Exception("FAILURE: "+getResult(inbox.getContent()));
            case ACLMessage.INFORM:
                this.Key=getResult(inbox.getContent());
                System.out.println("\nSubscribe correcto, key="+this.Key);
        }
        
        
    }
    //Se realiza el checkin y se devuelve el resultado al agente
    //COMPLETO
    private void altaAgente(AgentID idAgente,int rol){
        String nombre=idAgente.name;
        JsonObject json=new JsonObject();
        //Comprueba si el agente con ese nombre esta registrado
        if(agentes.containsKey(nombre)){
            json.add("result","ALREADY_REGISTERED");
            outbox = new ACLMessage(ACLMessage.REFUSE);
            outbox.setSender(this.id);
            outbox.setReceiver(idAgente);
            outbox.setContent(json.toString());
            send(outbox);
        }
        else{
            //Se añade un nuevo agente al hash, a la espera de saber sus coordenadas y otra info
            Agente age= new Agente(-100,-100,rol);
            age.ok=true;
            agentes.put(nombre, age);
            json.add("command", "checkin");
            json.add("rol", rol);
            json.add("key", Key);

            outbox = new ACLMessage(ACLMessage.REQUEST);
            outbox.setSender(idAgente);
            outbox.setReceiver(controlador);
            outbox.setContent(json.toString());
            send(outbox);
        }
        
        
    }
    
    private void bajaAgente(){
        
    } 
    
    //Funcion para obtener el campo result de un mensaje, hay muchos mensajes que contienen
    // el campo result, creo que esto lo simplificara
    private String getResult(String mensaje){
        JsonObject json=JsonObject.readFrom(mensaje);
        return json.get("result").asString();
    }
    
    //Espera a que lleguen mensajes a la cola
    private void esperarMensaje(boolean extra){
        while (cola.isEmpty()) { // Idle mientras no ha recibido nada. No bloqueante
            //System.out.println("\n[" + this.getName() + "] Idle");
            try {
                if(extra)
                    Thread.sleep(1000); // Espera 1 segundo hasta siguiente chequeo
                else
                    Thread.sleep(200);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    //Envia un mensaje CANCEL a controller
    private void cancelarSubscribe(boolean guardar){
        JsonObject mensajeJson = new JsonObject();
        mensajeJson.add("key", this.Key);
        outbox = new ACLMessage(ACLMessage.CANCEL);
        outbox.setSender(id);
        outbox.setReceiver(controlador);
        if(guardar){
            mensajeJson.add("trace", true);
            System.out.println("Enviando cancel con goal=true");
        }
        outbox.setContent(mensajeJson.toString());
        send(outbox);
        System.out.println("Cancelado");
    }
    
    private boolean esGoal(int x, int y){
        if(mapa[x][y]!=3)
            return false;
        if(mapa[x][y+1]==3 && mapa[x][y-1]==3)
            return true;
        if(mapa[x+1][y]==3 && mapa[x-1][y]==3)
            return true;
        
        return false;
    }
    
    /*
    METODO EXECUTE
    */
    public void execute(){
        try {
            System.out.println("Ejecutando subscribe");
            subscribe();
            interfaz.setVisible(true);
            while(!finalizado){
                //Se espera un mensaje, se recoge de la cola y se elige una accion:
                esperarMensaje(false);
                inbox=cola.Pop();
                AgentID botId=inbox.getSender();
                JsonObject json=JsonObject.readFrom(inbox.getContent());
                
                switch(inbox.getPerformativeInt()){
                    /*
                    REGISTRO DE UN BOT EN WM Y CHECKIN EN CONTROLLER
                    */
                    case Performativas.REGISTRO:
                        int rol=json.get("rol").asInt();
                        altaAgente(botId, rol);
                        break;
                    /*
                    BOT SOLICITA PERCIBIR
                    */
                    case Performativas.PERCIBIR:
                        JsonObject jsonAux=new JsonObject();
                        outbox=new ACLMessage(ACLMessage.QUERY_REF);
                        outbox.setSender(botId);
                        outbox.setReceiver(controlador);
                        jsonAux.add("key",this.Key);
                        outbox.setContent(jsonAux.toString());                        
                        send(outbox);
                        break;
                    /*
                    UN BOT HA REENVIADO SU MENSAJE DE PERCEPCION A WM
                    */
                    case Performativas.PERCEPCION:
                        JsonObject jsonDatos=json.get("result").asObject();
                        int x=jsonDatos.get("x").asInt();
                        int y=jsonDatos.get("y").asInt();
                        
                        Agente agenteBot =this.agentes.get(botId.name);
                        agenteBot.x=x;
                        agenteBot.y=y;
                        agenteBot.bateria=jsonDatos.get("battery").asInt();
                        boolean goal=jsonDatos.get("goal").asBoolean();
                        agenteBot.goal=goal;
                        agenteBot.mapaAgente=jsonDatos.get("sensor").asArray();
                        
                        //Se actualiza el mapa general de WM
                        actualizarMapa(agenteBot.mapaAgente, x, y);
                        if(goal){
                            goalX=x;
                            goalY=y;
                        }
                        //Con un mapa resulta que no se envia goal:true por lo que pongo esto
                        else if(esGoal(x, y)){
                            goalX=x;
                            goalY=y;
                        }
                        
                        this.energiaDisponible=jsonDatos.get("energy").asInt();
                        
                        ArrayList nombre = new ArrayList(agentes.keySet());
                        Agente agenteBot1 = this.agentes.get(nombre.get(0));
                        Agente agenteBot2 = this.agentes.get(nombre.get(1));
                        Agente agenteBot3 = this.agentes.get(nombre.get(2));
                        Agente agenteBot4 = this.agentes.get(nombre.get(3));
                        
                        try {;
                            interfaz.actualizarDrone(nombre.get(0).toString() ,agenteBot1.mapaAgente ,agenteBot1.rol ,agenteBot1.x ,agenteBot1.y ,agenteBot1.bateria);
                            interfaz.actualizarDrone(nombre.get(1).toString(),agenteBot2.mapaAgente, agenteBot2.rol, agenteBot2.x, agenteBot2.y, agenteBot2.bateria);
                            interfaz.actualizarDrone(nombre.get(2).toString(),agenteBot3.mapaAgente, agenteBot3.rol, agenteBot3.x, agenteBot3.y, agenteBot3.bateria);
                            interfaz.actualizarDrone(nombre.get(3).toString(),agenteBot4.mapaAgente, agenteBot4.rol, agenteBot4.x, agenteBot4.y, agenteBot4.bateria);
                            interfaz.actualizarWM(mapa, agenteBot1.x, agenteBot1.y, agenteBot2.x, agenteBot2.y, agenteBot3.x, agenteBot3.y, agenteBot4.x, agenteBot4.y,energiaDisponible);
                            //interfaz.repaint();
                        } catch (Exception e) {
                            System.err.println("ALGO HA FALLADO EN LA INTERFAZ");
                            e.printStackTrace();
                        }
                        break;
                    /*
                    BOT SOLICITANDO REALIZAR UN MOVIMIENTO, WM ENVIA EL MENSAJE A CONTROLLER SI SE AUTORIZA
                    Si se aproxima demasiado a otro se deniega
                    */
                    case Performativas.PROXIMOMOVIMIENTO:
                        //Comando moveX
                        String command=json.get("command").asString();
                        //Coordenadas de su posicion actual
                        int xActu=json.get("x").asInt();
                        int yActu=json.get("y").asInt();
                        //Se comprueba si se choca
                        if(validarMovimiento(botId.name, command, xActu, yActu)){
                            //Se envia un mensaje a controller como si fuera el bot
                            JsonObject jsonMov=new JsonObject();
                            jsonMov.add("command", command);
                            jsonMov.add("key",Key);
                            outbox= new ACLMessage(ACLMessage.REQUEST);
                            outbox.setContent(jsonMov.toString());
                            outbox.setReceiver(controlador);
                            outbox.setSender(botId);
                            send(outbox);
                        }
                        else{
                            outbox=new ACLMessage(ACLMessage.REFUSE);
                            outbox.setReceiver(botId);
                            outbox.setSender(id);
                            jsonAux=new JsonObject();
                            jsonAux.add("result", "BAD_MOVEMENT");
                            outbox.setContent(jsonAux.toString());
                            send(outbox);
                        }
                        break;
                    /*
                    BOT SOLICITANDO REFUEL
                    */
                    case Performativas.REFUEL:
                        if(validarRecarga(botId.name)){
                            json.add("key", Key);
                            outbox = new ACLMessage(ACLMessage.REQUEST);
                            outbox.setSender(botId);
                            outbox.setReceiver(controlador);
                            outbox.setContent(json.toString());

                        } else {
                            outbox = new ACLMessage(ACLMessage.REFUSE);
                            outbox.setSender(id);
                            outbox.setReceiver(botId);
                            outbox.setContent("DENIED");
                        }
                        send(outbox);
                        break;
                    /*
                    UN BOT INFORMANDO DEL RESULTADO DE UNA PETICION A CONTROLLER
                    */
                    case ACLMessage.INFORM:
                        try {
                            String result=json.get("result").asString();
                            Agente ag=agentes.get(botId.name);
                            ag.ok = result.equals("OK");
                        } catch (Exception e) {
                        }
                        break;
                        
                    /*
                    PETICIONES DE INFORMACION POR PARTE DE LOS BOTS
                    */
                    case ACLMessage.QUERY_REF:
                        String get=json.get("get").asString();
                        /*
                        INFORMACION DEL GOAL
                        */
                        switch(get){
                            case "goal":
                                outbox=new ACLMessage(ACLMessage.INFORM);
                                outbox.setReceiver(botId);
                                outbox.setSender(id);
                                JsonObject jsonGoal=new JsonObject();
                                //Si se ha localizado el objetivo se envian las coordenadas
                                if(goalX>=0 && goalY>=0){
                                    jsonGoal.add("goal",true);
                                    jsonGoal.add("x",goalX).add("y", goalY);
                                }
                                else{
                                    jsonGoal.add("goal",false);
                                }
                                outbox.setContent(jsonGoal.toString());
                                send(outbox);
                                break;
                            case "pos":
                                outbox = new ACLMessage(ACLMessage.INFORM);
                                outbox.setReceiver(botId);
                                outbox.setSender(id);
                                JsonObject jsonPos = new JsonObject();
                                JsonArray arrayPos=new JsonArray();
                                //Si se ha localizado el objetivo se envian las coordenadas
                                for (Map.Entry<String, Agente> entry : agentes.entrySet()) {
                                    String nom = entry.getKey();
                                    Agente age = entry.getValue();
                                    if(age.x>0 && age.y>0){
                                        arrayPos.add(age.x).add(age.y).add(age.rol);
                                    }
                                }
                                jsonPos.add("pos", arrayPos);
                                outbox.setContent(jsonPos.toString());
                                send(outbox);
                                break;
                        }
                        break;
                }
                /*
                Comprobamos si todos estan en el objetivo o sin bateria, si es asi
                hemos terminado y paramos el bucle
                */
                int terminados=0;
                for (Map.Entry<String, Agente> entry : agentes.entrySet()) {
                    String nombre = entry.getKey();
                    Agente agente= entry.getValue();
                    //Todos tienen que haber terminado
                    //System.err.println("<>"+nombre+": "+agente.goal);
                    if(agente.goal || agente.bateria==0 || !agente.ok)
                        terminados++;
                    
                }
                finalizado=(terminados==agentes.size());
                
            }
            System.out.println("¿GUARDAR TRAZA?(s/n) KEY="+this.Key);
            Scanner sc = new Scanner(System.in);
            String op=sc.nextLine();
            cancelarSubscribe(op.toUpperCase().equals("S"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    //Hebra para recibir y guardar los mensajes en una cola a la espera de ser tratados
    public void onMessage(ACLMessage msg)  {
        try {
            cola.Push(msg); // Cada mensaje nuevo que llega se encola en el orden de llegada
            System.out.println("\n["+this.getName()+"] Encolando: "+msg.getContent());
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}
