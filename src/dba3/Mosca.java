/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dba3;

import com.eclipsesource.json.JsonArray;
import es.upv.dsic.gti_ia.core.AgentID;

/**
 *
 * @author jorge
 */
public class Mosca extends Bot{

    public Mosca(AgentID id, AgentID WM, AgentID controller) throws Exception {
        super(id, WM, controller, 0);
        colorMensaje="\033[0;35m";
    }

     @Override
    public void heuristica() throws Exception{
        
        if (bateria <= 5) {
            System.out.println("Agente Mosca tiene hambre, recarga");
            try {
                refuel();
            } catch (Exception e) {
                System.err.println("##################\nError en llamar al refuel" + this.getName() + "->" + e.getMessage());
                e.printStackTrace(System.out);
            }
        }
        
        if(goal){
            System.out.println(">>>>GOAL");
            /*
            ALGORITMO PARA IR AL OBJETIVO 
            (NO ES SEGURO QUE LLEGE)
            */
            double dist=10000;
            int x_a=0,y_a=0;
            for (int i = y - 1; i < y + 2; i++) {
                for (int j = x - 1; j < x + 2; j++) {
                    //No se comprueba celda
                     //Si la celda esta libre
                    if (findemundo(j, i)) {
                        double distAux = distanciaGoal(j, i);
                        if (distAux < dist) {
                            dist = distAux;
                            x_a = j;
                            y_a = i;
                        }
                    }
                   
                        double distAux = distanciaGoal(j, i);
                        if (distAux < dist) {
                            dist = distAux;
                            x_a = j;
                            y_a = i;
                        }
                    
                }
            }//Fin del bucle anidado!!!!
            proxMov(x_a, y_a);
        }
        /*
        ALGORITMO DE BUSQUEDA
        Seleccionamos como la siguiente celda la que obtendriamos mas celdas exploradas
        
        else {
            //Siguientes coordenadas y las celdas nuevas que se exploraran
            int sigX=0,sigY=0,cont=-1;
            for (int i = y - 1; i < y + 2; i++) {
                for (int j = x - 1; j < x + 2; j++) {
                    //Si la celda esta libre
                   
                        int c=celdasExploradas(j, i);
                        if(c>cont){
                            cont=c;
                            sigX=j;
                            sigY=i;
                        
                    }
                }
            }
            explorado[sigY][sigX]=5;
            proxMov(sigX, sigY);
        }*/
              
    }
    
    
        
      private boolean findemundo(int x_a,int y_a){
        //Si la coordenada esta fuera de los limites
        if(x_a<0 || y_a<0 || x_a>=500 || y_a>=500)
            return false;
        //Si ya hay un agente(puede ser el mismo)
        if(explorado[y_a][x_a]==5)
            return false;
        if(x==x_a && y==y_a)
            return false;
        
        return true;
        
    }
    
     
    @Override
    public void execute() {
       try {
            registroWM();
            percibir();
            getPosicionesBots();
                if(esGoal(x, y)){
                    this.terminado=true;
                    System.out.println("TERMINADO MOSCAAAAAAAAA");
                }
                else{
               while (!terminado) {
                   percibir();
                    getPosicionesBots();
                    checkGoal();
                    heuristica();
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    
}
