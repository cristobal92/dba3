/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dba3;

import com.eclipsesource.json.JsonArray;
import es.upv.dsic.gti_ia.core.AgentID;

/**
 *
 * @author jorge
 */
public class Pajaro extends Bot{
    private boolean heuris1 = false;
    
    public Pajaro(AgentID id, AgentID WM, AgentID controller) throws Exception {
        super(id, WM, controller, 1);
        //color de mensajes azul
        colorMensaje="\033[0;35m";
        
    }
    public Pajaro(AgentID id, AgentID WM, AgentID controller, boolean heuris1) throws Exception {
        super(id, WM, controller, 1);
        //color de mensajes azul
        colorMensaje="\033[0;35m";
        this.heuris1=heuris1;
        
    }
    
    public void heuristicaBordes1() throws Exception{
        int x_a=0,y_a=0;
        if(bateria<=5)
            refuel();
        
        if(!primerMov){
            if(goal){
                imprimirMensaje(">>>>GOAL");
            /*
            ALGORITMO PARA IR AL OBJETIVO 
            (NO ES SEGURO QUE LLEGE)
            */
            double dist=10000;
           
            for (int i = y - 1; i < y + 2; i++) {
                for (int j = x - 1; j < x + 2; j++) {
                    //Si la celda esta libre
                    if (celdaLibre(j, i)) {
                        double distAux = distanciaGoal(j, i);
                        if(esGoal(j, i)){
                            proxMov(j, i);
                            return;
                        }
                        if (distAux < dist) {
                                dist = distAux;
                                x_a = j;
                                y_a = i;
                        }
                    }
                }
            }//Fin del bucle anidado!!!!
            proxMov(x_a, y_a);
        }
            
            else{
            if(posInicial==1){//Esta en el Norte
                if(movimientoInicial<=4){//COLOCA EL PAJARO EN LA POSICION INICIAL DE BUSQUEDA
                    if(mapaInterno[y+1][x] == 1){
                        x_a = x+1;
                        y_a = y;
                    }
                    else{    
                        x_a = x;
                        y_a = y+1;
                        movimientoInicial++;
                    }
                    proxMov(x_a, y_a);
                    direccion=3;
                }
                else if (direccion ==3){//COMIENZA HA BORDEAR EL MAPA en dirección este (3)
                    if(mapaInterno[y][x+2] == 2){
                        direccion = 2;
                         x_a = x;
                         y_a = y-1;
                    }else if(mapaInterno[y][x+1] == 1){
                        x_a = x;
                        y_a = y-1;
                    }
                    else{
                        x_a = x+1;
                        y_a = y;
                    }
                    proxMov(x_a, y_a);
                }
                else if (direccion ==2){//COMIENZA HA BORDEAR EL MAPA en dirección sur (2)
                    if(mapaInterno[y+2][x] == 2){
                        direccion = 4;
                         x_a = x-1;
                         y_a = y;
                    }else if(mapaInterno[y+1][x] == 1){
                        x_a = x+1;
                        y_a = y;
                    }
                    else{
                        x_a = x;
                        y_a = y+1;
                    }
                    proxMov(x_a, y_a);
                }
                
                else if (direccion ==4){//COMIENZA HA BORDEAR EL MAPA en dirección oeste (4)
                    if(mapaInterno[y][x-2] == 2){
                        direccion = 1;
                         x_a = x;
                         y_a = y+1;
                    }else if(mapaInterno[y][x-1] == 1){
                        x_a = x;
                        y_a = y+1;
                    }
                    else{
                        x_a = x-1;
                        y_a = y;
                    }
                    proxMov(x_a, y_a);
                }
                else if (direccion ==1){//COMIENZA HA BORDEAR EL MAPA en dirección Norte (1)
                    if(mapaInterno[y-2][x] == 2){
                        direccion = 3;
                         x_a = x+1;
                         y_a = y;
                    }else if(mapaInterno[y-1][x] == 1){
                        x_a = x-1;
                        y_a = y;
                    }
                    else{
                        x_a = x;
                        y_a = y-1;
                    }
                    proxMov(x_a, y_a);
                }
                
                
            }
 //--------------------------------------------------------------------------------------------
            else{//Esta en el SUR 
                if(movimientoInicial<=4){//COLOCA EL PAJARO EN LA POSICION INICIAL DE BUSQUEDA
                    if(mapaInterno[y-1][x] == 1){
                        x_a = x-1;
                        y_a = y;
                    }
                    else{    
                        x_a = x;
                        y_a = y-1;
                        movimientoInicial++;
                    }
                    proxMov(x_a, y_a);
                    direccion=4;
                }
                else if(direccion==4){//COMIENZA HA BORDEAR EL MAPA en direccion oeste (4)
                    if(x-2 == 0 ||mapaInterno[y][x-2] == 2){
                        direccion = 1;
                         x_a = x;
                         y_a = y-1;
                    }
                    else if(mapaInterno[y][x-1] == 1){
                        x_a = x;
                        y_a = y+1;
                    }
                    else{
                        x_a = x-1;
                        y_a = y;
                    }
                    proxMov(x_a, y_a);
                }
                else if(direccion==1){//COMIENZA HA BORDEAR EL MAPA en direccion norte (1)
                    if(y-2==0 || mapaInterno[y-2][x] == 2){
                        direccion = 3;
                         x_a = x+1;
                         y_a = y;
                    }
                    else if(mapaInterno[y-1][x] == 1){
                        x_a = x-1;
                        y_a = y;
                    }
                    else{
                        x_a = x;
                        y_a = y-1;
                    }
                    proxMov(x_a, y_a);
                }
                else if(direccion==3){//COMIENZA HA BORDEAR EL MAPA en direccion este (3)
                    if(mapaInterno[y][x+2] == 2){
                        direccion = 2;
                         x_a = x;
                         y_a = y+1;
                    }
                    else if(mapaInterno[y][x+1] == 1){
                        x_a = x;
                        y_a = y+1;
                    }
                    else{
                        x_a = x+1;
                        y_a = y;
                    }
                    proxMov(x_a, y_a);
                }
                else if(direccion==2){//COMIENZA HA BORDEAR EL MAPA en direccion sur (2)
                   if(mapaInterno[y+2][x] == 2){
                        direccion = 4;
                         x_a = x-1;
                         y_a = y;
                   }
                   else if(mapaInterno[y-1][x] == 1){
                        x_a = x+1;
                        y_a = y;
                    }
                   else{
                        x_a = x;
                        y_a = y+1;
                    }
                    proxMov(x_a, y_a);
                }
            }//CIERRE DEL ELSE ESTA AL SUR
            }    
        }  //CIERRE PRIMER IF
//------------------------------------------------------------------------------        
        
        else{//ELSE DEL PRIMER IF
            x_inicial=x;
            y_inicial=y;
            if(y<1)
                posInicial=1;
            else
                posInicial=2;
            primerMov=false;       
        }
    }
        
    

    @Override
    public void heuristica() throws Exception{
        if (esGoal(x, y)) {
            System.out.println("Agente Pajaro encontro el objetivo");
            terminado = true;
            return;
            //return "finalizado";
        } else if (bateria <= 5) {
            System.out.println("Agente Pajaro tiene hambre, se pilla un poco de alpiste");
            try {
                refuel();
            } catch (Exception e) {
                System.err.println("##################\nError en llamar al refuel" + this.getName() + "->" + e.getMessage());
                e.printStackTrace(System.out);
            }
        }
        
        if(goal){
            imprimirMensaje(">>>>GOAL");
            /*
            ALGORITMO PARA IR AL OBJETIVO 
            (NO ES SEGURO QUE LLEGE)
            */
            double dist=10000;
            int x_a=0,y_a=0;
            for (int i = y - 1; i < y + 2; i++) {
                for (int j = x - 1; j < x + 2; j++) {
                    //Si la celda esta libre
                    if (celdaLibre(j, i)) {
                        double distAux = distanciaGoal(j, i);
                        if(esGoal(j, i)){
                            proxMov(j, i);
                            return;
                        }
                        if (distAux < dist) {
                            dist = distAux;
                            x_a = j;
                            y_a = i;
                            }
                    }
                }
            }//Fin del bucle anidado!!!!
            proxMov(x_a, y_a);
        }
        /*
        ALGORITMO DE BUSQUEDA
        Seleccionamos como la siguiente celda la que obtendriamos mas celdas exploradas
        */
        else {
            //Siguientes coordenadas y las celdas nuevas que se exploraran
            int sigX=0,sigY=0,cont=-1;
            for (int i = y - 1; i < y + 2; i++) {
                for (int j = x - 1; j < x + 2; j++) {
                    //Si la celda esta libre
                    if( celdaLibre(j, i) ){
                        int c=celdasExploradas(j, i);
                        if(c>cont){
                            cont=c;
                            sigX=j;
                            sigY=i;
                        }
                    }
                }
            }//Fin del bucle anidado!!!!
            /*    for (int i = y; cont<=4 && i < explorado.length; i++) {
                    for (int j = x; cont<=4 && j < explorado[0].length; j++) {
                        if(explorado[i][j]==0){
                            sigX=j;
                            sigY=i;
                            cont=5;
                        }
                    }
                }
                    */
            //explorado[sigY][sigX]=5;
            proxMov(sigX, sigY);
        }
              
    }
    
      
    //Devuelve true si se puede mover a la celda
    private boolean celdaLibre(int x_a,int y_a){
        //Si la coordenada esta fuera de los limites
        if(x_a<0 || y_a<0 || x_a>=500 || y_a>=500)
            return false;
        //Si ya hay un agente(puede ser el mismo)
        if(explorado[y_a][x_a]==5)
            return false;
        if(x==x_a && y==y_a)
            return false;
        //Si es 0 o el objetivo se puede mover
        return this.mapaInterno[y_a][x_a]==0 || this.mapaInterno[y_a][x_a]==3;
        
    }
    

    @Override
    public void execute() {
        try {
            registroWM();
            while (!terminado) {
                percibir();
                getPosicionesBots();
                if(esGoal(x, y)){
                    this.terminado=true;
                }
                else{
                    checkGoal();
                    if(!heuris1)
                        heuristica();
                    else
                       heuristicaBordes1(); 
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
   
    

}
