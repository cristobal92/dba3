/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dba3;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import es.upv.dsic.gti_ia.core.ACLMessage;
import es.upv.dsic.gti_ia.core.SingleAgent;
import es.upv.dsic.gti_ia.core.AgentID;
import static java.lang.Math.sqrt;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author jorge
 * 
 */
public abstract class Bot extends SingleAgent{
    protected AgentID WM, controller,id;
    protected String key;
    protected int[][] mapaInterno;
    protected int[][] explorado;
    protected ACLMessage inbox, outbox;
    protected int x,y,rol,bateria;
    protected int direccion = 1;
    protected int movimientoInicial = 0;
    protected int posInicial=1;//1=Norte, 2=Sur
    protected int y_inicial, x_inicial;
    protected boolean primerMov = true;
    //Si se ha alcanzado al objetivo
    protected boolean terminado = false;
    //Indica si algun otro bot ha llegado al goal
    protected boolean goal=true;
    protected int goalX=50,goalY=48;
    //Si esta registrado
    protected boolean registrado=false;
    //Se guardan las coordenadas del ultimo movimiento no validado
    protected int invX=-1,invY=-1;
    //Array de posiciones de los otros agentes
    JsonArray posicionesBots;
    
    //Para la hora en los mensajes
    private Date date;
    private DateFormat hourFormat;
    protected String colorMensaje="\033[01;32m";
    
    public Bot(AgentID id, AgentID WM, AgentID controller,int rol) throws Exception{
        super(id);
        outbox = new ACLMessage();
        this.WM = WM;
        this.id=id;
        this.controller = controller;
        this.outbox = new ACLMessage();
        this.rol=rol;
        this.mapaInterno=new int[501][501];
        this.explorado=new int[501][501];
        /*
        for (int i = 0; i < explorado.length; i++) {
            for (int j = 0; j < explorado[0].length; j++) {
                explorado[i][j]=-1;
            }
        }
        */
        //Para poner hora en los mensajes
        date = new Date();
        hourFormat = new SimpleDateFormat("HH:mm:ss");
    }
    
    protected String getResult(String mensaje){
        JsonObject json=JsonObject.readFrom(mensaje);
        return json.get("result").asString();
    }
    
    protected void actualizarMapa(JsonArray radar) {
        //El numero de filas y columnas coinciden 
        final int tamM=mapaInterno.length;
        final int tamR=(int)sqrt(radar.size());
        /*
        El agente esta situado en el centro del mapa del radar, se obtienen unas 
        coordenadas en el mapa global donde comenzara a actualizarse
        */
        /*
        Las dimensiones son impares por lo que esta exactamente en la celda central,
        tamR se divide por dos, se trunca y se resta a las coordenadas en el mapa interno
        */ 
        int aux=(int)tamR/2;
        int i2 = y - aux;
        int j2 = x - aux;
        System.out.print("-------------------------------------------------------");
        //i,j->indices del radar
        //i2,j2->indices de la matriz mapaInterno
        for (int i = 0; i < tamR && i2 < tamM; i++, i2++) {
            System.out.print("\n");
            for (int j = 0; j < tamR && j2 < tamM; j++, j2++) {
                if (i2 >= 0 && j2 >= 0) {
                    mapaInterno[i2][j2] = radar.get(i * tamR + j).asInt();
                    if(esGoal(j2, i2)){
                        goal=true;
                        goalX=j2;
                        goalY=i2;
                    }
                    System.out.print(mapaInterno[i2][j2]);
                }
            }
            j2 -= tamR;
        }
    }
    //COMPLETO
    /*
    Realiza el registro en la WM, si se completa correctamente registrado=true
    */
    protected void registroWM() throws Exception{
        imprimirMensaje("Realizando registro en WM");
        JsonObject mensajeJson = new JsonObject();
        mensajeJson.add("rol", rol);
        outbox=new ACLMessage(Performativas.REGISTRO);
        outbox.setSender(id);
        outbox.setReceiver(WM);
        outbox.setContent(mensajeJson.toString());
        send(outbox);
        
        inbox=receiveACLMessage();
        String result=getResult(inbox.getContent());
        switch(inbox.getPerformativeInt()){
            case ACLMessage.INFORM:
                registrado=true;
                inbox.setSender(id);
                inbox.setReceiver(WM);
                send(inbox);
                imprimirMensaje("Registrado en WM y checkin en controller");
                break;
            default:
                throw new Exception(result);
                //break;
                
        }
    }
    
    //COMPLETO
    /*
    Solicita un refuel, true si se ha realizado, deberia solicitarse de nuevo percepcion
    para actualizar el nivel
    */
    protected boolean refuel() throws Exception{
        imprimirMensaje("Solicitando refuel");
        inbox=new ACLMessage(Performativas.REFUEL);
        JsonObject json=new JsonObject();
        json.add("command","refuel");
        inbox.setReceiver(WM);
        inbox.setSender(id);
        inbox.setContent(json.toString());
        send(inbox);
        
        inbox=receiveACLMessage();
        String result=getResult(inbox.getContent());
        if(inbox.getPerformativeInt()==ACLMessage.INFORM){
            imprimirMensaje("Refuel realizado");
            return true;
        }
        else if(result.equals("DENIED")){
            imprimirMensaje("Refuel denegado");
            return false;
        }
        else{
            outbox=inbox;
            outbox.setSender(id);
            outbox.setReceiver(WM);
            outbox.setPerformative(ACLMessage.INFORM);
            send(outbox);
            throw new Exception(result);
        }
    }
    //COMPLETO
    /*
    Solicita la percepcion y actualiza las variables, recordad que hay una variable goal indicando si ha llegado al objetivo
    */
    protected void percibir() throws Exception{
        imprimirMensaje("Solicitando percepcion");
        JsonObject mensajeJson = new JsonObject();
        mensajeJson.add("command", "percibir");
        outbox=new ACLMessage(Performativas.PERCIBIR);
        outbox.setSender(id);
        outbox.setReceiver(WM);
        outbox.setContent(mensajeJson.toString());
        send(outbox);
        
        inbox=receiveACLMessage();
        String content=inbox.getContent();
        if(inbox.getPerformativeInt()==ACLMessage.INFORM){
            imprimirMensaje("Percepcion recibida");
            JsonObject json=JsonObject.readFrom(content);
            //Objeto que contiene los datos de posicion, sensor etc
            JsonObject jsonDatos=json.get("result").asObject();
            //Coordenadas actuales
            this.x=jsonDatos.get("x").asInt();
            this.y=jsonDatos.get("y").asInt();
            this.bateria=jsonDatos.get("battery").asInt();
            //Si ha llegado al goal no necesita continuar
            this.terminado=jsonDatos.get("goal").asBoolean();
            
            //Actualizamos el mapa interno del agente
            actualizarMapa(jsonDatos.get("sensor").asArray());
            //Se reenvia el mensaje a WM para que actualize sus datos
            inbox.setPerformative(Performativas.PERCEPCION);
            inbox.setSender(id);
            inbox.setReceiver(WM);
            send(inbox);
        }
        else{
            outbox=inbox;
            outbox.setSender(id);
            outbox.setReceiver(WM);
            outbox.setPerformative(ACLMessage.INFORM);
            send(outbox);
            throw new Exception(getResult(content));
        }
    }
    
    //COMPLETADO
    /*
    Esta funcion realiza la solicitud de un movimiento a WM y procesa la respuesta
    devuelve true si se ha podido y false si ha estado demasiado cerca de otro
    */
    protected boolean proxMov(int xDest, int yDest)throws Exception{
        
        int x_b=xDest-x;
        int y_b=yDest-y;
        //Se ira añadiendo a result la direccion que corresponda con el signo de x_b e y_b
        String command="move";
        if(y_b<0){
            command+="N";
        }
        else if(y_b>0){
            command+="S";
        }
        if(x_b<0){
            command+="W";
        }
        else if(x_b>0){
            command+="E";
        }
        imprimirMensaje("Solicitando moverse a x="+xDest+" y="+yDest);
        JsonObject json=new JsonObject();
        json.add("command", command);
        //En el mensaje se envia la posicion actual, por si las moscas
        json.add("x",x).add("y",y);
        outbox=new ACLMessage(Performativas.PROXIMOMOVIMIENTO);
        outbox.setContent(json.toString());
        outbox.setSender(id);
        outbox.setReceiver(WM);
        send(outbox);
        
        inbox=receiveACLMessage();
        String result=getResult(inbox.getContent());
        if(inbox.getPerformativeInt()==ACLMessage.INFORM){
            x=xDest;
            y=yDest;
            imprimirMensaje("Se ha relizado el movimiento, posicion actual: x="+x+" y="+y);
            invX=-1;
            invY=-1;
            return true;
        }
        else if(result.equals("BAD_MOVEMENT")){
            imprimirMensaje("No se realiza movimiento BAD_MOVEMENT");
            invX=xDest;
            invY=yDest;
            return false;
        }
        else{
            outbox=inbox;
            outbox.setSender(id);
            outbox.setReceiver(WM);
            outbox.setPerformative(ACLMessage.INFORM);
            send(outbox);
            throw new Exception(result);
        }
    }
    
    //Funcion que solicita informacion sobre la localizacion del objetivo
    protected void checkGoal() throws Exception{
        //Si lo ha encontrado no necesita mandar la peticion
        if(!goal){
            JsonObject json = new JsonObject();
            json.add("get", "goal");
            outbox = new ACLMessage(ACLMessage.QUERY_REF);
            outbox.setContent(json.toString());
            outbox.setSender(id);
            outbox.setReceiver(WM);
            send(outbox);
            
            inbox=receiveACLMessage();
            json=JsonObject.readFrom(inbox.getContent());
            if(json.get("goal").asBoolean()){
            
                this.goal=true;
                goalX=json.get("x").asInt();
                goalY=json.get("y").asInt();
            }
        }
    }
    protected int celdasExploradas(int x_, int y_){
        final int tamM=explorado.length;
        int contador=0;
        int tamR=1;
        switch(rol){
            case 0:
                tamR=3;
                break;
            case 1:
                tamR=5;
                break;
            case 2:
                tamR=11;
        }
        /*
        El agente esta situado en el centro del mapa del radar, se obtienen unas 
        coordenadas en el mapa global donde comenzara a actualizarse
        */
        /*
        Las dimensiones son impares por lo que esta exactamente en la celda central,
        tamR se divide por dos, se trunca y se resta a las coordenadas en el mapa interno
        */ 
        int aux=(int)tamR/2;
        int i2 = y_  - aux;
        int j2 = x_ - aux;
        System.err.print("-------------------------------------------------------");
        //i,j->indices del radar
        //i2,j2->indices de la matriz mapaInterno
        for (int i = 0; i < tamR && i2 < tamM; i++, i2++) {
            for (int j = 0; j < tamR && j2 < tamM; j++, j2++) {
                if (i2 >= 0 && j2 >= 0 && explorado[i2][j2]==0) {
                    contador++;
                }
            }
            j2 -= tamR;
        }
        System.out.println(">>>>"+contador);
        return contador;
    }
    protected void actualizarExploracion(int rol, int xA, int yA){
        final int tamM=explorado.length;
        int tamR=1;
        switch(rol){
            case 0:
                tamR=3;
                break;
            case 1:
                tamR=5;
                break;
            case 2:
                tamR=11;
        }
        /*
        El agente esta situado en el centro del mapa del radar, se obtienen unas 
        coordenadas en el mapa global donde comenzara a actualizarse
        */
        /*
        Las dimensiones son impares por lo que esta exactamente en la celda central,
        tamR se divide por dos, se trunca y se resta a las coordenadas en el mapa interno
        */ 
        int aux=(int)tamR/2;
        int i2 = yA - aux;
        int j2 = xA - aux;
        System.err.print("-------------------------------------------------------");
        //i,j->indices del radar
        //i2,j2->indices de la matriz mapaInterno
        for (int i = 0; i < tamR && i2 < tamM; i++, i2++) {
            System.err.print("\n");
            for (int j = 0; j < tamR && j2 < tamM; j++, j2++) {
                if (i2 >= 0 && j2 >= 0) {
                    explorado[i2][j2] = 1;
                    System.err.print(mapaInterno[i2][j2]);
                }
            }
            j2 -= tamR;
        }
    }
    
    protected void getPosicionesBots() throws Exception{
        JsonObject json = new JsonObject();
            json.add("get", "pos");
            outbox = new ACLMessage(ACLMessage.QUERY_REF);
            outbox.setContent(json.toString());
            outbox.setSender(id);
            outbox.setReceiver(WM);
            send(outbox);
            
            inbox=receiveACLMessage();
            //Borramos las antiguas posiciones
            
            for (int i = 0; posicionesBots!=null && i < posicionesBots.size()/3; i++) {
                int xAg=posicionesBots.get(i*3).asInt();
                int yAg=posicionesBots.get(i*3+1).asInt();
                explorado[yAg][xAg]=1;
            }
            
            posicionesBots=JsonObject.readFrom(inbox.getContent()).get("pos").asArray();
            for (int i = 0; i < posicionesBots.size()/3; i++) {
                int xAg=posicionesBots.get(i*3).asInt();
                int yAg=posicionesBots.get(i*3+1).asInt();
                int rolAg=posicionesBots.get(i*3+2).asInt();
                actualizarExploracion(rolAg, xAg, yAg);
                //5 significa que hay un bot
                explorado[yAg][xAg]=5;
            }
    }
    
    //funcion que devuelve la distancia de un punto al goal si este se ha localizado
    //Si no devuelve -1
    protected double distanciaGoal(int x_,int y_){
        if(!goal)
            return -1;
        double X=goalX-x_;
        double Y=goalY-y_;
        return (sqrt(X*X+Y*Y));
    }
    protected boolean esGoal(int x, int y){
        if(mapaInterno[x][y]!=3)
            return false;
        if(mapaInterno[x][y+1]==3 && mapaInterno[x][y-1]==3)
            return true;
        if(mapaInterno[x+1][y]==3 && mapaInterno[x-1][y]==3)
            return true;
        
        return false;
    }
    
    protected void imprimirMensaje(String msg) {
        System.out.println(colorMensaje+hourFormat.format(date) + "-> " + this.getName() + ": " + msg);
    }
    
    public abstract void heuristica() throws Exception;
            
    public abstract void execute();

}
