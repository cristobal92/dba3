/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dba3;

import es.upv.dsic.gti_ia.core.AgentID;
import es.upv.dsic.gti_ia.core.AgentsConnection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jorge
 */
public class DBA3 {

    /**
     * @param args the command line arguments
     * Roles:
     * 0->mosca
     * 1->pajaro
     * 2->halcon
     */
    private static String mundos[]=new String[]{"beach","pyrenees","alps","andes","valleys","faun","everest","newyork"};
    
    public static void main(String[] args) {
        AgentsConnection.connect("siadex.ugr.es", 6000, "Andromeda", "Aries", "Picasso", false);
        AgentID idController=new AgentID("Andromeda");
        String aleatorio="new";
        AgentID idManager=new AgentID("world_manager"+aleatorio);
        AgentID idPajaro=new AgentID("Agente1"+aleatorio);
        AgentID idDron1=new AgentID("Agente2"+aleatorio);
        AgentID idDron2=new AgentID("Agente3"+aleatorio);
        AgentID idDron3=new AgentID("Agente4"+aleatorio);
        try {
            WorldManager manager=new WorldManager(idManager, idController, mundos[0]);
            Bot pajaro=new Mosca(idPajaro, idManager, idController);
            Bot pajaro2 = new Pajaro(idDron1,idManager,idController);
            Bot pajaro3 = new Pajaro(idDron2,idManager,idController,true);
            Bot pajaro4 = new Halcon(idDron3,idManager,idController);
            manager.start();
            pajaro.start();
            pajaro2.start();
            pajaro3.start();
            pajaro4.start();
        } catch (Exception ex) {
            Logger.getLogger(DBA3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
