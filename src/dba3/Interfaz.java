/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dba3;

import com.eclipsesource.json.JsonArray;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.color.ColorSpace;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 *
 * @author panilla
 */
public class Interfaz extends javax.swing.JFrame{
    
    private Mapa mapaWM;
    private Mapa mapaDrone1;
    private Mapa mapaDrone2;
    private Mapa mapaDrone3;
    private Mapa mapaDrone4;
    private JPanel panelMapas;
    private JPanel panelDatos;
    private JScrollPane panelScrollWM;
    private JProgressBar bateriaD1;
    private JProgressBar bateriaD2;
    private JProgressBar bateriaD3;
    private JProgressBar bateriaD4;
    private JLabel nombreBateriaWM;
    private JLabel bateriaWM;
    private JLabel nombreBateriaD1;
    private JLabel nombreGPSD1;
    private JLabel nombreXD1;
    private JLabel nombreYD1;
    private JLabel valorXD1;
    private JLabel valorYD1;
    private JLabel nombreBateriaD2;
    private JLabel nombreGPSD2;
    private JLabel nombreXD2;
    private JLabel nombreYD2;
    private JLabel valorXD2;
    private JLabel valorYD2;
    private JLabel nombreBateriaD3;
    private JLabel nombreGPSD3;
    private JLabel nombreXD3;
    private JLabel nombreYD3;
    private JLabel valorXD3;
    private JLabel valorYD3;
    private JLabel nombreBateriaD4;
    private JLabel nombreGPSD4;
    private JLabel nombreXD4;
    private JLabel nombreYD4;
    private JLabel valorXD4;
    private JLabel valorYD4;
    
    
    public Interfaz(){
        initComponents();
        this.setMinimumSize(new Dimension(1150, 500));
        this.setMaximumSize(new Dimension(1150, 600));
        this.setTitle("Practica 3 DBA");
    }

    private void initComponents() {
        /*
          int delay = 500; //milliseconds
  ActionListener taskPerformer = new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                  repaint();
              }
  };
  new Timer(delay, taskPerformer).start();
  */
  
        mapaWM = new Mapa(2505,2505,5,5);
        mapaDrone1= new Mapa(165,165,15,15);
        mapaDrone2 = new Mapa(165,165,15,15);
        mapaDrone3 = new Mapa(165,165,15,15);
        mapaDrone4 = new Mapa(165,165,15,15);
        panelMapas = new JPanel();
        panelDatos = new JPanel();
        panelScrollWM = new JScrollPane();
        bateriaD1 = new JProgressBar();
        bateriaD2 = new JProgressBar();
        bateriaD3 = new JProgressBar();
        bateriaD4 = new JProgressBar();
        nombreBateriaWM = new JLabel();
        nombreBateriaWM.setText("Nivel de Bateria: ");
        bateriaWM = new JLabel();
        bateriaWM.setText("");
        
        nombreBateriaD1 = new JLabel();
        nombreBateriaD1.setText("Bateria: ");
        
        nombreGPSD1 = new JLabel();
        nombreGPSD1.setText("GPS: ");
        
        nombreXD1 = new JLabel();
        nombreXD1.setText("X");
        nombreYD1 = new JLabel();
        nombreYD1.setText("Y");
        valorXD1 = new JLabel();
        valorXD1.setText("0");
        valorYD1 = new JLabel();
        valorYD1.setText("0");
        
        nombreBateriaD2 = new JLabel();
        nombreBateriaD2.setText("Bateria: ");
        
        nombreGPSD2 = new JLabel();
        nombreGPSD2.setText("GPS: ");
        
        nombreXD2 = new JLabel();
        nombreXD2.setText("X");
        nombreYD2 = new JLabel();
        nombreYD2.setText("Y");
        valorXD2 = new JLabel();
        valorXD2.setText("0");
        valorYD2 = new JLabel();
        valorYD2.setText("0");
        
        nombreBateriaD3 = new JLabel();
        nombreBateriaD3.setText("Bateria: ");
        
        nombreGPSD3 = new JLabel();
        nombreGPSD3.setText("GPS: ");
                
        nombreXD3 = new JLabel();
        nombreXD3.setText("X");
        nombreYD3 = new JLabel();
        nombreYD3.setText("Y");
        valorXD3 = new JLabel();
        valorXD3.setText("0");
        valorYD3 = new JLabel();
        valorYD3.setText("0");
        
        nombreBateriaD4 = new JLabel();
        nombreBateriaD4.setText("Bateria: ");
        
        nombreGPSD4 = new JLabel();
        nombreGPSD4.setText("GPS: ");
        
        nombreXD4 = new JLabel();
        nombreXD4.setText("X");
        nombreYD4 = new JLabel();
        nombreYD4.setText("Y");
        
        valorXD4 = new JLabel();
        valorXD4.setText("0");
        valorYD4 = new JLabel();
        valorYD4.setText("0");
        
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panelMapas.setMaximumSize(new java.awt.Dimension(550, 3000));

        panelScrollWM.setBorder(javax.swing.BorderFactory.createTitledBorder("WorldManager"));

        mapaWM.setBackground(Color.GRAY);
        mapaWM.setPreferredSize(new java.awt.Dimension(500, 500));
        panelScrollWM.setPreferredSize(new java.awt.Dimension(500,500));
        mapaWM.setVerifyInputWhenFocusTarget(false);

        javax.swing.GroupLayout mapaWMLayout = new javax.swing.GroupLayout(mapaWM);
        mapaWM.setLayout(mapaWMLayout);
        mapaWMLayout.setHorizontalGroup(
            mapaWMLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 528, Short.MAX_VALUE)
        );
        mapaWMLayout.setVerticalGroup(
            mapaWMLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 516, Short.MAX_VALUE)
        );

        panelScrollWM.setViewportView(mapaWM);

        javax.swing.GroupLayout panelMapasLayout = new javax.swing.GroupLayout(panelMapas);
        panelMapas.setLayout(panelMapasLayout);
        panelMapasLayout.setHorizontalGroup(
            panelMapasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMapasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelScrollWM, javax.swing.GroupLayout.PREFERRED_SIZE, 540, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelMapasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(nombreBateriaWM)
                .addComponent(bateriaWM)
                )
        );
        panelMapasLayout.setVerticalGroup(
            panelMapasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMapasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelScrollWM, javax.swing.GroupLayout.PREFERRED_SIZE, 540, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(nombreBateriaWM)
                .addComponent(bateriaWM)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mapaDrone2.setBackground(new java.awt.Color(254, 247, 247));
        mapaDrone2.setBorder(javax.swing.BorderFactory.createTitledBorder("Dron1"));

        javax.swing.GroupLayout mapaDrone2Layout = new javax.swing.GroupLayout(mapaDrone2);
        mapaDrone2.setLayout(mapaDrone2Layout);
        mapaDrone2Layout.setHorizontalGroup(
            mapaDrone2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 165, Short.MAX_VALUE)
        );
        mapaDrone2Layout.setVerticalGroup(
            mapaDrone2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 165, Short.MAX_VALUE)
        );

        mapaDrone1.setBackground(new java.awt.Color(254, 247, 247));
        mapaDrone1.setBorder(javax.swing.BorderFactory.createTitledBorder("Dron1"));

        javax.swing.GroupLayout mapaDrone1Layout = new javax.swing.GroupLayout(mapaDrone1);
        mapaDrone1.setLayout(mapaDrone1Layout);
        mapaDrone1Layout.setHorizontalGroup(
            mapaDrone1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 165, Short.MAX_VALUE)
        );
        mapaDrone1Layout.setVerticalGroup(
            mapaDrone1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 165, Short.MAX_VALUE)
        );

        mapaDrone4.setBackground(new java.awt.Color(254, 247, 247));
        mapaDrone4.setBorder(javax.swing.BorderFactory.createTitledBorder("Dron1"));

        javax.swing.GroupLayout mapaDrone4Layout = new javax.swing.GroupLayout(mapaDrone4);
        mapaDrone4.setLayout(mapaDrone4Layout);
        mapaDrone4Layout.setHorizontalGroup(
            mapaDrone4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 165, Short.MAX_VALUE)
        );
        mapaDrone4Layout.setVerticalGroup(
            mapaDrone4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 165, Short.MAX_VALUE)
        );

        mapaDrone3.setBackground(new java.awt.Color(254, 247, 247));
        mapaDrone3.setBorder(javax.swing.BorderFactory.createTitledBorder("Dron1"));

        javax.swing.GroupLayout mapaDrone3Layout = new javax.swing.GroupLayout(mapaDrone3);
        mapaDrone3.setLayout(mapaDrone3Layout);
        mapaDrone3Layout.setHorizontalGroup(
            mapaDrone3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 165, Short.MAX_VALUE)
        );
        mapaDrone3Layout.setVerticalGroup(
            mapaDrone3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 165, Short.MAX_VALUE)
        );
        
        javax.swing.GroupLayout panelDatosLayout = new javax.swing.GroupLayout(panelDatos);
        panelDatos.setLayout(panelDatosLayout);
        panelDatosLayout.setHorizontalGroup(
            panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDatosLayout.createSequentialGroup()
                .addGap(59, 59, 59)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelDatosLayout.createSequentialGroup()
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nombreGPSD4)
                            .addGroup(panelDatosLayout.createSequentialGroup()
                                .addComponent(nombreXD4)
                                .addGap(18, 18, 18)
                                .addComponent(nombreYD4))
                            .addGroup(panelDatosLayout.createSequentialGroup()
                                .addComponent(valorXD4)
                                .addGap(18, 18, 18)
                                .addComponent(valorYD4))
                            .addComponent(nombreBateriaD4)
                            .addComponent(bateriaD4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 144, Short.MAX_VALUE)
                        .addComponent(mapaDrone4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelDatosLayout.createSequentialGroup()
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nombreGPSD3)
                            .addGroup(panelDatosLayout.createSequentialGroup()
                                .addComponent(nombreXD3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(nombreYD3))
                            .addGroup(panelDatosLayout.createSequentialGroup()
                                .addComponent(valorXD3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(valorYD3))
                            .addComponent(nombreBateriaD3)
                            .addComponent(bateriaD3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(mapaDrone3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelDatosLayout.createSequentialGroup()
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nombreGPSD2)
                            .addGroup(panelDatosLayout.createSequentialGroup()
                                .addComponent(nombreXD2)
                                .addGap(18, 18, 18)
                                .addComponent(nombreYD2))
                            .addGroup(panelDatosLayout.createSequentialGroup()
                                .addComponent(valorXD2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(valorYD2))
                            .addComponent(nombreBateriaD2)
                            .addComponent(bateriaD2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(mapaDrone2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelDatosLayout.createSequentialGroup()
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelDatosLayout.createSequentialGroup()
                                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(nombreGPSD1)
                                    .addComponent(nombreXD1)
                                    .addComponent(valorXD1)
                                    .addComponent(nombreBateriaD1))
                                .addGap(18, 18, 18)
                                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(nombreYD1)
                                    .addComponent(valorYD1)))
                            .addComponent(bateriaD1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(mapaDrone1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        panelDatosLayout.setVerticalGroup(
            panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDatosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mapaDrone1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelDatosLayout.createSequentialGroup()
                        .addComponent(nombreGPSD1)
                        .addGap(15, 15, 15)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nombreXD1)
                            .addComponent(nombreYD1))
                        .addGap(18, 18, 18)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(valorXD1)
                            .addComponent(valorYD1))
                        .addGap(18, 18, 18)
                        .addComponent(nombreBateriaD1)
                        .addGap(14, 14, 14)
                        .addComponent(bateriaD1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mapaDrone2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelDatosLayout.createSequentialGroup()
                        .addComponent(nombreGPSD2)
                        .addGap(19, 19, 19)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nombreXD2)
                            .addComponent(nombreYD2))
                        .addGap(16, 16, 16)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(valorXD2)
                            .addComponent(valorYD2))
                        .addGap(22, 22, 22)
                        .addComponent(nombreBateriaD2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(bateriaD2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mapaDrone3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelDatosLayout.createSequentialGroup()
                        .addComponent(nombreGPSD3)
                        .addGap(19, 19, 19)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nombreXD3)
                            .addComponent(nombreYD3))
                        .addGap(14, 14, 14)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(valorXD3)
                            .addComponent(valorYD3))
                        .addGap(22, 22, 22)
                        .addComponent(nombreBateriaD3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(bateriaD3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mapaDrone4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelDatosLayout.createSequentialGroup()
                        .addComponent(nombreGPSD4)
                        .addGap(18, 18, 18)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nombreXD4)
                            .addComponent(nombreYD4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(valorXD4)
                            .addComponent(valorYD4))
                        .addGap(18, 18, 18)
                        .addComponent(nombreBateriaD4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(bateriaD4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelMapas, javax.swing.GroupLayout.PREFERRED_SIZE, 550, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelDatos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelMapas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panelDatos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }
     
    public void setBateriaWM(double valor){
        
        bateriaWM.setText(""+valor);
    }
    
    public void setBateria(JProgressBar bateria ,double valor){
        
        bateria.setValue((int)valor);
        if(valor > 50){
            bateria.setForeground(Color.green);
        }
        else
            if(valor>10){
                bateria.setForeground(Color.yellow);
            }
            else{
                bateria.setForeground(Color.red);
            }
        
    }
  
    public void actualizarWM(int[][] mapa, int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int energia){
        
        for(int i = 0; i<501; i++){
            for(int j = 0; j<501;j++){
                
                this.mapaWM.set(i,j,mapa[j][i]);
            }
        }
        
        this.mapaWM.set(x1,y1,4);
        this.mapaWM.set(x2,y2,5);
        this.mapaWM.set(x3,y3,6);
        this.mapaWM.set(x4,y4,7);
        
        setBateriaWM(energia);
        mapaWM.repaint();
       
    }
    
    public void actualizarDrone(String nombre,JsonArray mapaDrone,int tipo, int x, int y, int bateria){
        
        int tama=5;
                
            switch(tipo){
                case 0: tama=3;
                    break;
                case 2: tama=11;
                    break;
            }
        switch(nombre.substring(0, 7)){
            
            case "Agente1":
                
                mapaDrone1.setBorder(javax.swing.BorderFactory.createTitledBorder("Agente1"));
                for(int j = 0; j<tama; j++){
                    for(int i = 0; i<tama;i++){

                    this.mapaDrone1.setValor(j,i,mapaDrone.get(i*tama+j).asInt(),tipo);
                    }  
                }
                this.mapaDrone1.setValor(tama/2,tama/2,6,tipo);


                    valorXD1.setText(""+x);
                    valorYD1.setText(""+y);
                    setBateria(bateriaD1, bateria);
                    mapaDrone1.repaint();
            break;
            case "Agente2":
           
                mapaDrone2.setBorder(javax.swing.BorderFactory.createTitledBorder("Agente2"));
                for(int j = 0; j<tama; j++){
                    for(int i = 0; i<tama;i++){

                        this.mapaDrone2.setValor(j,i,mapaDrone.get(i*tama+j).asInt(),tipo);
                        }
                    }
                    this.mapaDrone2.setValor(tama/2,tama/2,5,tipo);

            valorXD2.setText(""+x);
            valorYD2.setText(""+y);
            setBateria(bateriaD2, bateria);
            mapaDrone2.repaint();
            break;
            case "Agente3":
            
                mapaDrone3.setBorder(javax.swing.BorderFactory.createTitledBorder("Agente3"));              
                for(int j = 0; j<tama; j++){
                   for(int i = 0; i<tama;i++){

                            this.mapaDrone3.setValor(j,i,mapaDrone.get(i*tama+j).asInt(),tipo);
                            }
                        }
                        this.mapaDrone3.setValor(tama/2,tama/2,4,tipo);


                    valorXD3.setText(""+x);
                    valorYD3.setText(""+y);
                    setBateria(bateriaD3, bateria);
                    mapaDrone3.repaint();
            break;         
            case "Agente4":
                
                mapaDrone4.setBorder(javax.swing.BorderFactory.createTitledBorder("Agente4"));
                for(int j = 0; j<tama; j++){
                    for(int i = 0; i<tama;i++){

                    this.mapaDrone4.setValor(j,i,mapaDrone.get(i*tama+j).asInt(),tipo);
                    }
                }
                this.mapaDrone4.setValor(tama/2,tama/2,7,tipo);

                    valorXD4.setText(""+x);
                    valorYD4.setText(""+y);
                    setBateria(bateriaD4, bateria);
                    mapaDrone4.repaint();
            break;
        }
        
    }
    
   
    
    
    
    
    
    
}
    
    
